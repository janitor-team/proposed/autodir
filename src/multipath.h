#ifndef MULTIPATH_H
#define MULTIPATH_H

void multipath_init(void);

int multipath_inc(const char *name);
int multipath_dec(const char *name);

#endif
